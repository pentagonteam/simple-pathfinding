# README #

This project use in Unity 2D board game.
With this package, you can find the closet path from start to end point.

### What is this repository for? ###

* Version 1.0


### How do I get set up? ###


* Download this package
* Import or copy the folder 'PathFinding' to anywhere you want your asset scripts folder

### How to use ###

```
#!c#

// Create the tiles map
float[,] tilesmap = new float[width, height];
// Set values here....
// Every float in the array represent the cost of passing the tile at that position.
// Use 0.0f for blocking tiles.

// Create a grid
PathFind.Grid grid = new PathFind.Grid(width, height, tilesmap);

// Create source and target points
PathFind.Point _from = new PathFind.Point(1, 1);
PathFind.Point _to = new PathFind.Point(10, 10);


// Path will either be a list of Points (x, y), or an empty list if no path is found. 
// It is include diagonally line
List<PathFind.Point> path = PathFind.Pathfinding.FindPath(grid, _from, _to);

// Path will either be a list of Points (x, y), or an empty list if no path is found. 
// It is NOT include diagonally line
List<PathFind.Point> path = PathFind.Pathfinding.FindPath(grid, _from, _to, true);
```

If you don't care about price of tiles (eg tiles can only be walkable or blocking), you can also pass a 2d array of booleans when creating the grid:

```
#!c#

// create the tiles map
bool[,] tilesmap = new bool[width, height];
// set values here....
// true = walkable, false = blocking

// create a grid
PathFind.Grid grid = new PathFind.Grid(width, height, tilesmap);

// rest is the same..
```